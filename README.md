# The Gamebot Avatar #

A fun fancy Gamebot to run around and adventure with on the metaverse! This avatar comes in a tablet app that allows you to change your appearance, face expression and colour. Pick and choose what you like!

### Features ###

* Comes with tablet app to easily change your appearance.
* 10 Different colour options (For the body, backpack and heart) to choose from. Mix and match them to make a unique Gamebot of your own!