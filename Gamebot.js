/*
	Gamebot Applier App. Made by Kurtis Anatine (Vargink)
	Get in contact with me via https://keybase.io/theguywho


*/
(function() {
	// Adding app naming convention to make sure calls are not grabbed from other things
	var app = "TC_Gamebot";
	// stuff that i should just do once and totally not every damn time i need it #StuckInLSL
	var urlIndex = Script.resolvePath("index.html");
	var iconIndex = Script.resolvePath("icons/gamebot.svg");
	var bodyIndex = Script.resolvePath("Gamebot.fst");
	var timerCheck = null;
	var gamebotInit = false;
	// That sweet tablet stuff
	var tablet = Tablet.getTablet("com.highfidelity.interface.tablet.system");
	var button = tablet.addButton({
		text: "GAMEBOT",
		icon: iconIndex,
		activeIcon: iconIndex
	});
	// pull your settings if you have them. If not make a blank object with the standard avatar.
	var gamebot = Settings.getValue("gamebot");
	if (gamebot == "") {
		gamebot = {
			avBase: bodyIndex,
			body: Script.resolvePath("Gamebot/body/Yellow.fbx"),
			backpack: Script.resolvePath("Gamebot/backpack/Green.fbx"),
			heart: Script.resolvePath("Gamebot/backpackheart/Red.fbx"),
			glasses: Script.resolvePath("Gamebot/glasses/Classic.fbx"),
			face: Script.resolvePath("Gamebot/faces/Face05.fbx")
		};
	} else {
		gamebot = JSON.parse(gamebot);
	}
	function trimURL(url) {
		url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
		return url;
	}
	// Seems like detachOne fails silently so i didnt bother with a sanity check if the attachment exists. Might wanna do that later kurtinator.
	function changeAttachment(part,replacement) {
		switch(part) {
			case "body":
				if (gamebot.body != "None") {
					MyAvatar.detachOne(gamebot.body);
				}
				if (replacement != "None") {
					MyAvatar.attach(replacement,"Gamebot Base",{"x": 0, "y": 0.25, "z": 0},{"x": 0, "y": 0, "z": 0, "w": 1},1,true);
				}
				gamebot.body = replacement;
				return true;
			case "backpack":
				if (gamebot.backpack != "None") {
					MyAvatar.detachOne(gamebot.backpack);
				}
				if (replacement != "None") {
					MyAvatar.attach(replacement,"Gamebot Base",{"x": 0, "y": 0.25, "z": 0},{"x": 0, "y": 0, "z": 0, "w": 1},1,true);
					if (gamebot.heart != "None" && gamebot.backpack == "None") {
						MyAvatar.attach(gamebot.heart,"Gamebot Base",{"x": 0, "y": 0.25, "z": 0},{"x": 0, "y": 0, "z": 0, "w": 1},1,true);
					}
				} else if (gamebot.heart != "None") {
					MyAvatar.detachOne(gamebot.heart);
				}
				gamebot.backpack = replacement;
				return true;
			case "heart":
				if (gamebot.backpack != "None") {
					if (gamebot.heart != "None") {
						MyAvatar.detachOne(gamebot.heart);
					}
					if (replacement != "None") {
						MyAvatar.attach(replacement,"Gamebot Base",{"x": 0, "y": 0.25, "z": 0},{"x": 0, "y": 0, "z": 0, "w": 1},1,true);
					}
				}
				gamebot.heart = replacement;
				return true;
			case "glasses":
				if (gamebot.glasses != "None") {
					MyAvatar.detachOne(gamebot.glasses);
				}
				if (replacement != "None") {
					MyAvatar.attach(replacement,"Gamebot Base",{"x": 0, "y": 0.25, "z": 0},{"x": 0, "y": 0, "z": 0, "w": 1},1,true);
				}
				gamebot.glasses = replacement;
				return true;
			case "face":
				if (gamebot.face != "None") {
					MyAvatar.detachOne(gamebot.face);
				}
				if (replacement != "None") {
					MyAvatar.attach(replacement,"Gamebot Base",{"x": 0, "y": 0.25, "z": 0},{"x": 0, "y": 0, "z": 0, "w": 1},1,true);
				}
				gamebot.face = replacement;
				return true;
			case "avBase":
			// There will be a prompt to ask if you would like to wear this avatar when this value it hit. So we will need to check when the avatar url has been changed to the value before we throw any attachments onto it
				gamebot.avBase = replacement;
				if (timerCheck != null) {
					Script.clearInterval(timerCheck);
					timerCheck = null;
				}
				timerCheck = Script.setInterval(function() {
					if (trimURL(MyAvatar.skeletonModelURL) == trimURL(bodyIndex)) {
						changeAttachment("body",gamebot.body);
						changeAttachment("backpack",gamebot.backpack);
						changeAttachment("heart",gamebot.heart);
						changeAttachment("glasses",gamebot.glasses);
						changeAttachment("face",gamebot.face);
						tablet.emitScriptEvent(
							JSON.stringify({
								cmd: "avbody",
								value: true
							}));
						Script.clearInterval(timerCheck);
						timerCheck = Script.setInterval(function() {
							if (trimURL(MyAvatar.skeletonModelURL) != trimURL(bodyIndex)) {
								MyAvatar.detachOne(gamebot.body);
								MyAvatar.detachOne(gamebot.backpack);
								MyAvatar.detachOne(gamebot.heart);
								MyAvatar.detachOne(gamebot.glasses);
								MyAvatar.detachOne(gamebot.face);
								Script.clearInterval(timerCheck);
								timerCheck = null;
							}
						}, 5000);
					}
				}, 5000);
				return true;
			case "exit":
				tablet.gotoHomeScreen();
				return false;
			case "init":
				var avState = (trimURL(MyAvatar.skeletonModelURL) == trimURL(bodyIndex));
				tablet.emitScriptEvent(
					JSON.stringify({
						cmd: "avbody",
						value: avState
					}));
				if (avState && timerCheck == null) {
					// the base av is on and the avatar change timer isnt active. Better start that back up (this could be if the gamebots been changed back outside of the app)
					timerCheck = Script.setInterval(function() {
							if (trimURL(MyAvatar.skeletonModelURL) != trimURL(bodyIndex)) {
								MyAvatar.detachOne(gamebot.body);
								MyAvatar.detachOne(gamebot.backpack);
								MyAvatar.detachOne(gamebot.heart);
								MyAvatar.detachOne(gamebot.glasses);
								MyAvatar.detachOne(gamebot.face);
								Script.clearInterval(timerCheck);
								timerCheck = null;
							}
						}, 5000);
				}
				return false;
		}
	}
	// Init Function is here eyy lmao
	function Start() {
		function onClicked() {
			tablet.gotoWebScreen(urlIndex);
		}
		button.clicked.connect(onClicked);

		function buttonHit(event) {
			if (typeof event === "string") {
				event = JSON.parse(event);
				if (event.app == app) { // Since event calls are fired all the time might be a good idea to make identifiers for all my functions. Saves having to worry about conflics
					if (changeAttachment(event.part,event.file)) {
						Settings.setValue("gamebot",JSON.stringify(gamebot));
					}
				}
			}
		}
		tablet.webEventReceived.connect(buttonHit);
		function cleanup() {
			tablet.removeButton(button);
		}
		Script.scriptEnding.connect(cleanup);
	}

	// Kick the whole thing into action
	Start();
}());