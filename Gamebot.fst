name = Gamebot
type = body+head
scale = 1
filename = Gamebot/Gamebot.fbx
texdir = Gamebot/textures
joint = jointEyeRight = RightEye
joint = jointEyeLeft = LeftEye
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandRing4_end = 28
jointIndex = Hips = 1
jointIndex = RightHandThumb4_end = 43
jointIndex = LeftHandMiddle4 = 61
jointIndex = LeftHandThumb4 = 71
jointIndex = RightHandRing4 = 27
jointIndex = LeftEyeSocket = 78
jointIndex = LeftToe = 10
jointIndex = RightEyeSocket = 75
jointIndex = LeftHandMiddle2 = 59
jointIndex = LeftUpLeg = 7
jointIndex = RightHandMiddle2 = 30
jointIndex = LeftHandIndex4_end = 67
jointIndex = Neck = 73
jointIndex = RightHandMiddle1 = 29
jointIndex = LeftEye_end = 80
jointIndex = LeftHandPinky4_end = 52
jointIndex = LeftHandRing3 = 55
jointIndex = RightHand = 18
jointIndex = RightHandPinky1 = 19
jointIndex = RightToe_end = 6
jointIndex = RightToe = 5
jointIndex = RightHandIndex1 = 34
jointIndex = LeftToe_end = 11
jointIndex = RightLeg = 3
jointIndex = LeftHandPinky4 = 51
jointIndex = LeftHandMiddle1 = 58
jointIndex = Head = 74
jointIndex = LeftHand = 47
jointIndex = LeftHandMiddle4_end = 62
jointIndex = RightHandIndex3 = 36
jointIndex = RightEye_end = 77
jointIndex = RightShoulder = 15
jointIndex = RightForeArm = 17
jointIndex = RightHandPinky4 = 22
jointIndex = RightHandPinky2 = 20
jointIndex = LeftLeg = 8
jointIndex = RightHandRing1 = 24
jointIndex = LeftHandRing4 = 56
jointIndex = RightHandIndex2 = 35
jointIndex = LeftHandThumb3 = 70
jointIndex = RightArm = 16
jointIndex = LeftHandPinky1 = 48
jointIndex = LeftHandPinky3 = 50
jointIndex = LeftHandRing2 = 54
jointIndex = LeftHandPinky2 = 49
jointIndex = LeftHandRing4_end = 57
jointIndex = RightHandRing3 = 26
jointIndex = Spine1 = 13
jointIndex = LeftHandThumb2 = 69
jointIndex = LeftHandRing1 = 53
jointIndex = LeftHandIndex1 = 63
jointIndex = RightEye = 76
jointIndex = RightHandThumb2 = 40
jointIndex = RightHandIndex4 = 37
jointIndex = LeftHandIndex4 = 66
jointIndex = LeftFoot = 9
jointIndex = LeftEye = 79
jointIndex = LeftHandThumb1 = 68
jointIndex = RightHandIndex4_end = 38
jointIndex = LeftHandThumb4_end = 72
jointIndex = Armature = 0
jointIndex = LeftHandIndex2 = 64
jointIndex = Spine2 = 14
jointIndex = RightHandPinky4_end = 23
jointIndex = LeftShoulder = 44
jointIndex = RightFoot = 4
jointIndex = RightHandMiddle3 = 31
jointIndex = Spine = 12
jointIndex = LeftArm = 45
jointIndex = RightUpLeg = 2
jointIndex = RightHandRing2 = 25
jointIndex = RightHandThumb1 = 39
jointIndex = RightHandThumb3 = 41
jointIndex = RightHandPinky3 = 21
jointIndex = LeftHandIndex3 = 65
jointIndex = LeftForeArm = 46
jointIndex = LeftHandMiddle3 = 60
jointIndex = RightHandThumb4 = 42
jointIndex = RightHandMiddle4 = 32
jointIndex = RightHandMiddle4_end = 33
